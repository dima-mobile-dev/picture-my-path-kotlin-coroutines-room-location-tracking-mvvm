package com.andromeda.picturemypath.data.repository

import com.andromeda.picturemypath.common.Result
import com.andromeda.picturemypath.data.PhotoRepositoryImpl
import com.andromeda.picturemypath.data.database.PhotoDao
import com.andromeda.picturemypath.data.database.PhotoEntity
import com.andromeda.picturemypath.data.network.FlickrClient
import com.andromeda.picturemypath.data.network.PhotoResponseEntity
import com.andromeda.picturemypath.data.network.toDomainModel
import com.andromeda.picturemypath.data.network.toPhotoEntity
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PhotoRepositoryImplTest {

    private lateinit var repository: PhotoRepositoryImpl

    private val mockFlickrClient: FlickrClient = mock()
    private val mockPhotoDatabase: PhotoDao = mock()

    private val lat = "lat"
    private val lon = "lon"
    private val radius = "0.1"
    private val id1 = "id1"
    private val id2 = "id2"
    private val secret = "secret"
    private val server = "server"
    private val farm = "farm"
    private val photoResponseEntity1 = PhotoResponseEntity(id1, secret, server, farm)
    private val photoEntity1 = photoResponseEntity1.toPhotoEntity()
    private val photoResponseEntity2 = PhotoResponseEntity(id2, secret, server, farm)
    private val photoEntity2 = photoResponseEntity2.toPhotoEntity()

    @Before
    fun setUp() {
        repository = PhotoRepositoryImpl(mockFlickrClient, mockPhotoDatabase)
    }

    @Test
    fun `given empty database, when searchPhotoByLocation, then return the first photo`() {
        runBlocking {
            val photosFromDb = arrayOf<PhotoEntity>()
            whenever(mockPhotoDatabase.selectAllPhotos()).thenReturn(photosFromDb)

            val photosFromFlickr = listOf(photoResponseEntity1)
            val result = Result.Success(photosFromFlickr)
            whenever(mockFlickrClient.searchPhoto(lat, lon, radius)).thenReturn(result)
            doNothing().`when`(mockPhotoDatabase).insert(photoEntity1)

            val test = repository.searchPhotoByLocation(lat, lon)

            verify(mockFlickrClient).searchPhoto(lat, lon, radius)
            Assert.assertEquals(test, Result.Success(result.data[0].toDomainModel()))
        }
    }

    @Test
    fun `given filled database, when searchPhotoByLocation returns existing photo, then return next photo`() {
        runBlocking {
            val photosFromDb = arrayOf<PhotoEntity>(photoEntity1)
            whenever(mockPhotoDatabase.selectAllPhotos()).thenReturn(photosFromDb)

            val photosFromFlickr = listOf(photoResponseEntity1, photoResponseEntity2)
            val result = Result.Success(photosFromFlickr)
            whenever(mockFlickrClient.searchPhoto(lat, lon, radius)).thenReturn(result)
            doNothing().`when`(mockPhotoDatabase).insert(photoEntity2)

            val test = repository.searchPhotoByLocation(lat, lon)

            verify(mockFlickrClient).searchPhoto(lat, lon, radius)
            Assert.assertEquals(test, Result.Success(result.data[1].toDomainModel()))
        }
    }

}
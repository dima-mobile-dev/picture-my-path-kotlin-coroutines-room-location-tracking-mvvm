package com.andromeda.picturemypath.data.network

import com.andromeda.picturemypath.common.Result

interface FlickrClient {

    suspend fun searchPhoto(
        lat: String,
        lon: String,
        radius: String
    ): Result<List<PhotoResponseEntity>>
}

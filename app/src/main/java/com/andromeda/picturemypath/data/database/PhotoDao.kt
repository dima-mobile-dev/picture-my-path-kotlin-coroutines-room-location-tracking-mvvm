package com.andromeda.picturemypath.data.database

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Insert
import androidx.room.OnConflictStrategy

@Dao
interface PhotoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(photo: PhotoEntity)

    @Query("SELECT * FROM photos ORDER BY photoId DESC")
    fun selectAllPhotos(): Array<PhotoEntity>

    @Query("DELETE FROM photos")
    fun deletePhotos()
}

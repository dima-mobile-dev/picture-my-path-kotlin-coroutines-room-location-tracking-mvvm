package com.andromeda.picturemypath.presentation.model

import android.os.Parcelable

import kotlinx.parcelize.Parcelize

import com.andromeda.picturemypath.domain.model.Photo

@Parcelize
data class PhotoViewItem(
    val id: String,
    val secret: String,
    val server: String,
    val farm: String
) : Parcelable

fun Photo.toPresentationModel() = PhotoViewItem(
    id = id,
    secret = secret,
    server = server,
    farm = farm
)

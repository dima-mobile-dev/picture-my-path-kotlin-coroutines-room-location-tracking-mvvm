package com.andromeda.picturemypath.domain.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import com.andromeda.picturemypath.common.Result
import com.andromeda.picturemypath.domain.PhotoRepository
import com.andromeda.picturemypath.domain.model.Photo

class RetrievePhotosUseCase(private val photoRepository: PhotoRepository) {

    suspend operator fun invoke(): Result<List<Photo>> = withContext(Dispatchers.IO) {
        return@withContext photoRepository.loadAllPhotos()
    }
}

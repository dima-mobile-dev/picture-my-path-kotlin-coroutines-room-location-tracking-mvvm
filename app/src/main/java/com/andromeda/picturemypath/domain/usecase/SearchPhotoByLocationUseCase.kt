package com.andromeda.picturemypath.domain.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

import com.andromeda.picturemypath.common.Result
import com.andromeda.picturemypath.domain.PhotoRepository
import com.andromeda.picturemypath.domain.model.Photo

class SearchPhotoByLocationUseCase(private val photoRepository: PhotoRepository) {

    suspend operator fun invoke(lat: Double, lon: Double): Result<Photo> =
        withContext(Dispatchers.IO) {
            return@withContext photoRepository.searchPhotoByLocation(lat.toString(), lon.toString())
        }
}

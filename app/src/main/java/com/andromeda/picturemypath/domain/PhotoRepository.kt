package com.andromeda.picturemypath.domain

import com.andromeda.picturemypath.common.Result
import com.andromeda.picturemypath.domain.model.Photo

// Repository modules handle data operations.
// They provide a clean API so that the rest of the app can retrieve this data easily.
interface PhotoRepository {

    suspend fun searchPhotoByLocation(lat: String, lon: String): Result<Photo>

    suspend fun loadAllPhotos(): Result<List<Photo>>

    suspend fun deletePhotos()
}

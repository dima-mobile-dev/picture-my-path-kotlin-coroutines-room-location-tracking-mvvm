package com.andromeda.picturemypath.domain

import com.andromeda.picturemypath.common.Result
import com.andromeda.picturemypath.domain.model.Photo
import com.andromeda.picturemypath.domain.usecase.ClearPhotosUseCase
import com.andromeda.picturemypath.domain.usecase.SearchPhotoByLocationUseCase

class LocationServiceInteractor(
    private val clearPhotosUseCase: ClearPhotosUseCase,
    private val searchPhotoByLocationUseCase: SearchPhotoByLocationUseCase
) {

    suspend fun clearPhotosFromList() {
        clearPhotosUseCase.invoke()
    }

    suspend fun getPhotoBasedOnLocation(latitude: Double, longitude: Double): Result<Photo> {
        return searchPhotoByLocationUseCase.invoke(latitude, longitude)
    }
}
